﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_n01330783
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
          
        }

        protected void quadrantCheck(object sender, EventArgs e)
        {
            int inputy = int.Parse(userInputY.Text);
            int inputx = Convert.ToInt32(userInputX.Text);

            Inputxy newinput = new Inputxy(inputx, inputy);

            if (newinput.inputx > 0 && newinput.inputy > 0)
            {
                output.InnerHtml = "Falls in first Quadrant";
            }
            else if (newinput.inputx < 0 && newinput.inputy > 0)
            {
                output.InnerHtml = "Falls in second Quadrant";
            }
            else if (newinput.inputx < 0 && newinput.inputy < 0)
            {
                output.InnerHtml = "Falls in third Quadrant";
            }
            else if (newinput.inputx > 0 && newinput.inputy < 0)
            {
                output.InnerHtml = "Falls in fourth Quadrant";
            }
        }
    }
}