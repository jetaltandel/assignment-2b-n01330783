﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_n01330783
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void checkWord(object sender, EventArgs e)
        {
            string word = inputWord.Text;
            string allword = word.ToLower();
            allword = allword.Replace(" ", string.Empty);
            string reversWord = "";

            for (int i = allword.Length - 1; i >= 0; i--)
            {
                reversWord += allword[i].ToString();
            }
            if (reversWord == allword)
            {
                outputWord.InnerHtml = "String is palindrome";
            }
            else
            {
                outputWord.InnerHtml = "String is not palindrome";
            }
        }
    }
}