﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment_n01330783.Cartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cartesian Smartesian</title>
</head>
<body>
    <h1>Cartesian Smartesian</h1>
    <form id="form1" runat="server">
        <div>
         <label>Input X:</label><br />
          <asp:TextBox runat="server" ID="userInputX"></asp:TextBox>
         <asp:CompareValidator runat="server" ControlToValidate="userInputX" Operator="NotEqual" 
              ValueToCompare="0" ErrorMessage="0 is not a valid input"></asp:CompareValidator>
            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[-+]?\d*$" ControlToValidate="userInputX" ErrorMessage="Must be integer">
          </asp:RegularExpressionValidator>
            </div>
        <div>
          <label>Input Y:</label><br />
           <asp:TextBox runat="server" ID="userInputY"></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToValidate="userInputY" Operator="NotEqual" 
              ValueToCompare="0" ErrorMessage="0 is not a valid input"></asp:CompareValidator>
            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[-+]?\d*$" ControlToValidate="userInputY" ErrorMessage="Must be integer">
          </asp:RegularExpressionValidator>
        </div><br />
        
        <asp:Button runat="server" ID="submitButton" OnClick="quadrantCheck" Text="Submit"/>
        <div runat="server" id="output">
            
         </div>
    </form>
</body>
</html>
