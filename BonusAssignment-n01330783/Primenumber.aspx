﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Primenumber.aspx.cs" Inherits="BonusAssignment_n01330783.Primenumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrimeNumber</title>
</head>
<body>
    <h1>Check number is prime or not</h1>
    <form id="form1" runat="server">
        <div>
            
         <label>Enter Number</label><br />
          <asp:TextBox runat="server" ID="userNumber"></asp:TextBox>
         <asp:CompareValidator runat="server" ControlToValidate="userNumber" Operator="GreaterThan" 
              ValueToCompare="0" ErrorMessage="Must be positive value"></asp:CompareValidator>
            <asp:RegularExpressionValidator runat="server" ValidationExpression="^[-+]?\d*$" ControlToValidate="userNumber" ErrorMessage="Must be integer">
          </asp:RegularExpressionValidator>
            
        </div><br />
        <asp:Button runat="server" ID="submitButton" OnClick="checkNumber" Text="Submit"/>
        <div runat="server" id="output">
            
         </div>
    </form>
</body>
</html>
