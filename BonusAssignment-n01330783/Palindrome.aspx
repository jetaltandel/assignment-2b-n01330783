﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignment_n01330783.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Palindrome word</title>
</head>
<body>
    <h1>Check palindrome word</h1>
    <form id="form1" runat="server">
        <div>
            <label>Enter input</label><br />
          <asp:TextBox runat="server" ID="inputWord"></asp:TextBox>
         <asp:RegularExpressionValidator runat="server" Type="String" ValidationExpression="^[a-zA-Z ]+$" ControlToValidate="inputWord" 
             ErrorMessage="Must be string"></asp:RegularExpressionValidator>
        </div><br/>
        <asp:Button runat="server" ID="submitButton" OnClick="checkWord"  Text="Submit"/>
        <div runat="server" id="outputWord">
            
         </div>
    </form>
</body>
</html>
